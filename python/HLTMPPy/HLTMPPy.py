from builtins import str
from past.builtins import basestring
from builtins import object
import os
import sys
import xml.etree.ElementTree as et

import logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
log = logging.getLogger('HLTMPPy')

# Temporary hack to get the code work in both python2 and python3.
# In python3 tostring returns an object with type bytes
def et_tostring(elem):
  if sys.version_info[0] < 3:
    return et.tostring(elem)
  else:
    return et.tostring(elem, encoding="unicode")

def prettyxml(elem,indent_level = 0, indent_increment = 2):
    """ Standalone recursive implementation of pretty_xml """
    indent = " " * indent_level
    finalstr = ""
    if len(elem) == 0: # No child node, just print it in single line
        return indent + et_tostring(elem)
    else:
        newindent = indent_level + indent_increment
        finalstr += indent + "<{}>\n".format(elem.tag)
        for el in sorted(elem, key=lambda child : child.tag):
            finalstr += prettyxml(el, newindent, indent_increment = indent_increment) + "\n"
        finalstr += indent + "</{}>".format(elem.tag)
    return finalstr

def recurseDict(d,rootName):
    r=et.Element(rootName)
    if isinstance(d,dict):
        for k in iter(d):
            val=d[k]
            if isinstance(val,basestring):
                et.SubElement(r,k).text=str(val)
            elif isinstance(val,list):
                for el in val:
                    rr = et.Element(k)
                    rr.text=el
                    r.append(rr)
            elif isinstance(val,dict):
                l=recurseDict(val,k)
                r.append(l)
            else:
                log.warning("Element isn't list, dict or str as expected: {}".format(val))

    elif isinstance(d,basestring):
        r.text=d
    elif isinstance(d,list):
        for val in d:
            r.append(recurseDict(val,rootName))
    else:
        log.warning("Input to recurseDict isn't list, dict or str as expected: {}".format(d))
    return r

def genRosMapping(ros2robMap):
    ''' Convert ros2robMap python dictionary to xml tree
    Format has to be
    ros2robMap={"ROS-FULL_SD_EVENT-00":[1120005,1120006], "ROS-FULL_SD_EVENT-01":[2120005,2120006]}
    '''
    root=et.Element("ROS2ROBS")
    for ros in iter(ros2robMap):
        robs=ros2robMap[ros]
        currentros = et.Element(ros)
        for rob in robs:
            et.SubElement(currentros,"ROBID").text=str(rob)
        root.append(currentros)
    return root

class DataSource(object):
    def __init__(self,library):
        self._library=library
        self._defaultDict={}

    def getLibrary(self):
        return self._library
    def getDefaultDict(self):
        return self._defaultDict
    def getTree(self):
        return None

class DCMDataSource(DataSource):
    def __init__(self,library="dfinterfaceDcm"):
        DataSource.__init__(self,library)
        self._defaultDict={
            # "HLTDFDCMBackend":{
                "UID" : "DataSource-is-DCM",
                "library" : str(self._library)
            # }
        }

    def getTree(self):
        return recurseDict({"HLTDFDCMBackend":self._defaultDict},"DataSource")

class DFFileDataSource(DataSource):
    def __init__(self,library="DFFileBackend",
                 fileList=[],
                 outFile=None,
                 rosHitStatFile="ros_hitstats",
                 compressionFormat="ZLIB",
                 compressionLevel=2,
                 numEvents=-1,
                 loopFiles="false",
                 preload="false",
                 extraL1Robs=[],
                 skipEvents=0):
        self._fileList=fileList
        DataSource.__init__(self,library)
        self._defaultDataDict={
            "UID" : "DataSource-is-DCM",
            "library" : str(self._library),
            "loopOverFiles": loopFiles,
            "start_id":1,
            "preload": preload,
            "numEvents": numEvents,
            "fileOffset":-1,
            "compressionLevel":compressionLevel,
            "compressionFormat":compressionFormat,
            "fileList":self._fileList,
            "rosHitStatFileName":rosHitStatFile,
            "extraL1Robs":  extraL1Robs,
            "skipEvents":skipEvents
        }
        if outFile is not None:
            self._defaultDataDict["outputFileName"]=outFile
        self._defaultDict={
            "HLTDFFileBackend" : self._defaultDataDict
        }

    def getTree(self):
        root=et.Element("DataSource")
        ds=et.SubElement(root,"HLTDFFileBackend")
        plainlist=[x for x in list(self._defaultDataDict.keys()) if x != "fileList" and x != "extraL1Robs"]
        for k in plainlist:
            et.SubElement(ds,k).text=str(self._defaultDataDict[k])
        flist=et.SubElement(ds,"fileList")
        files=self._defaultDataDict["fileList"]
        for f in files:
            et.SubElement(flist,"file").text=str(f)
        extraL1RobList=et.SubElement(ds,"extraL1Robs")
        robs=self._defaultDataDict["extraL1Robs"]
        for r in robs:
            et.SubElement(extraL1RobList,"ROBID").text=str(r)
        return root

class InfoService(object):
    def __init__(self,libraryName):
        self._library=libraryName
        self._defaultDict={}
    def getLibrary(self):
        return self._library
    def getDefaultDictionary(self):
        return self._defaultDict
    def getTree(self):
        return None

class MonSvcInfoService(InfoService):
    def __init__(self,libraryName="MonSvcInfoService",
                 OHServer="${TDAQ_OH_SERVER=Histogramming}",
                 OHSlots=1,
                 OHInterval=80,
                 OHInclude=".*",
                 OHExclude="",
                 ISServer="${TDAQ_IS_SERVER=DF}",
                 ISSlots=1,
                 ISInterval=5,
                 ISRegex=".*"
    ):
        InfoService.__init__(self,libraryName)
        self._defaultDict={
            "UID":"hltMonSvc",
            "library":"MonSvcInfoService",
            "ConfigurationRules":[
                {
                    "UID":"HltpuConfigurationRuleBundle",
                    "Rules" : [
                        {
                            "UID":"HltpuOHRule",
                            "IncludeFilter":str(OHInclude),
                            "ExcludeFilter":str(OHExclude),
                            "Name":"Dumm",
                            "Parameters":{
                                "OHPublishingParameters":{
                                    "UID":"HltpuOHPublishingParameters",
                                    "PublishInterval":str(OHInterval),
                                    "OHServer":str(OHServer),
                                    "NumberOfSlots":str(OHSlots),
                                    "ROOTProvider":"${TDAQ_APPLICATION_NAME}"
                                }
                            }
                        },
                        {
                            "UID":"HltpuISRule",
                            "IncludeFilter":str(ISRegex),
                            "ExcludeFilter":"",
                            "Name":"DummDumm",
                            "Parameters":{
                                "ISPublishingParameters":{
                                    "UID":"HltpuISPublishingParameters",
                                    "PublishInterval":str(ISInterval),
                                    "NumberOfSlots":str(ISSlots),
                                    "ISServer":str(ISServer)
                                }
                            }
                        }
                    ]
                }
            ]
        }
    def getTree(self):
        root=et.Element("HLTMonInfoImpl")
        plainlist=[x for x in iter(self._defaultDict) if x != "ConfigurationRules" ]
        for k in plainlist:
            et.SubElement(root,k).text=str(self._defaultDict[k])
        crl=et.SubElement(root,"ConfigurationRules")
        cr=self._defaultDict["ConfigurationRules"]
        for f in cr:
            crb=et.Element("ConfigurationRuleBundle")
            et.SubElement(crb,"UID").text=str(f["UID"])
            rules=et.SubElement(crb,"Rules")
            for r in f["Rules"]:
                #crn=et.SubElement(rules,"ConfigurationRule")
                crn=recurseDict(r,"ConfigurationRule")
                rules.append(crn)
            #et.SubElement(flist,"file").text=str(f)
            crl.append(crb)
        return root

class TriggerConfig(object):
    def __init__(self):
        self._defaultDict={}
    def getDefaultLibrary(self):
        return None
    def getDBConfig(self,SMK=0,coral=False,srv="LOCAL_HOST",port=3320,
                    user="ATLAS_CONF_TRIGGER_RUN2_R",pwd="TrigConfigRead2015",alias="TRIGGERDB"):
        TC={"TriggerDBConnection":
            {
                "UID":"TriggerDB_RUN2_CoralServer_Example",
                "Server":"%s"%(srv),
                "Port":"%s"%(port),
                "Name":"'&oracle://ATLAS_CONFIG/ATLAS_CONF_TRIGGER_RUN2'",
                "Alias":str(alias),
                "User":"%s"%(user),
                "Password":"%s"%(pwd),
                "Type":"Coral",
                "SuperMasterKey":"%s"%(SMK)
            }
        }

        if coral:
            defaultConns= [
                {
                    "UID": "ATLAS_COOLONL_INDET_CORALSRV",
                    "Server": "%s"%(srv),
                    "Port" :"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_INDET'",
                    "Alias": "COOLONL_INDET",
                    "User": "''",
                    "Password": "''",
                    "Type": "Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_MDT_CORALSRV",
                    "Server": "%s"%(srv),
                    "Port": "%s"%(port),
                    "Name": "'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_MDT'",
                    "Alias":"COOLONL_MDT",
                    "User": "''",
                    "Password" :"''",
                    "Type": "Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_SCT_CORALSRV",
                    "Server": "%s"%(srv),
                    "Port": "%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_SCT'",
                    "Alias":"COOLONL_SCT",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_TRT_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_TRT'",
                    "Alias":"COOLOFL_TRT",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_RPC_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_RPC'",
                    "Alias":"COOLONL_RPC",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_TDAQ_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_TDAQ'",
                    "Alias":"COOLONL_TDAQ",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_MUONALIGN_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_MUONALIGN'",
                    "Alias":"COOLONL_MUONALIGN",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_LAR_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_LAR'",
                    "Alias":"COOLONL_LAR",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLASDD_CORALSRV_THROUGHATLASDD",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_DD/ATLASDD'",
                    "Alias":"ATLASDD",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_PIXEL_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_PIXEL'",
                    "Alias":"COOLONL_PIXEL",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_MDT_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_MDT'",
                    "Alias":"COOLOFL_MDT",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_CALO_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_CALO'",
                    "Alias":"COOLONL_CALO",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_CSC_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_CSC'",
                    "Alias":"COOLONL_CSC",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_TRT_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_TRT'",
                    "Alias":"COOLONL_TRT",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOL_GLOBAL_ORACLE",
                    "Server":"ATLAS_COOLPROD",
                    "Port":"''",
                    "Name":"ATLAS_COOLONL_GLOBAL",
                    "Alias":"COOLONL_GLOBAL",
                    "User":"ATLAS_COOL_READER_U",
                    "Password":"LMXTPRO4RED",
                    "Type":"Oracle"
                },
                {
                    "UID":"ATLAS_COOLONL_GLOBAL_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_GLOBAL'",
                    "Alias":"COOLONL_GLOBAL",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_PIXEL_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_PIXEL'",
                    "Alias":"COOLOFL_PIXEL",
                    "User":"ATLAS_COOL_PIXEL",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_TILE_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_TILE'",
                    "Alias":"COOLOFL_TILE",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                    },
                {
                    "UID":"ATLAS_COOLOFL_INDET_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_INDET'",
                    "Alias":"COOLOFL_INDET",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                    },
                {
                    "UID":"ATLAS_COOLONL_TRIGGER_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_TRIGGER'",
                    "Alias":"COOLONL_TRIGGER",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_CSC_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_CSC'",
                    "Alias":"COOLOFL_CSC",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_SCT_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_SCT'",
                    "Alias":"COOLOFL_SCT",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_LAR_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_LAR'",
                    "Alias":"COOLOFL_LAR",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLASDD_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://atlas_dd/atlasdd'",
                    "Alias":"ATLASDD",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_TGC_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_TGC'",
                    "Alias":"COOLONL_TGC",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_DCS_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_DCS'",
                    "Alias":"COOLOFL_DCS",
                    "User":"ATLAS_COOL_DCS",
                    "Password":"''",
                    "Type":"Coral"
                    },
                {
                    "UID":"ATLAS_COOLONL_MUON_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_MUON'",
                    "Alias":"COOLONL_MUON",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLOFL_GLOBAL_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLOFL_GLOBAL'",
                    "Alias":"COOLOFL_GLOBAL",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                },
                {
                    "UID":"ATLAS_COOLONL_TILE_CORALSRV",
                    "Server":"%s"%(srv),
                    "Port":"%s"%(port),
                    "Name":"'&oracle://ATLAS_COOLPROD/ATLAS_COOLONL_TILE'",
                    "Alias":"COOLONL_TILE",
                    "User":"''",
                    "Password":"''",
                    "Type":"Coral"
                }
            ]

            root=et.Element("DBConnections")
            for dbc in defaultConns:
                c=et.SubElement(root,"DBConnection")
                for k in list(dbc.keys()):
                    et.SubElement(c,str(k)).text=str(dbc[k])
            return (root,recurseDict(TC,"TriggerDBConnection"))
        return (None,recurseDict(TC,"TriggerDBConnection"))

    def getTree(self):
        return  recurseDict(self._defaultDict,"TriggerConfiguration")

class TriggerConfigJO(TriggerConfig):
    def __init__(self,jopath,SMK=0,prescaleKey=0,bunchKey=0,L1MenuFrom="DB",preCmds=[],postCmds=[],joType="JobOptionsSvc",msgType="TrigMessageSvc",pythonSetupFile="TrigPSC/TrigPSCPythonSetup.py",logLevels=["INFO","ERROR"]):
        TriggerConfig.__init__(self)

        self._defaultDict={
            "TriggerConfiguration": {
                "UID": "JobOptionsTriggerConfig-1",
                "L1TriggerConfiguration": {
                    "L1TriggerConfiguration": {
                        "UID":"L1TrigConf",
                        "Lvl1PrescaleKey":"%s"%(prescaleKey),
                        "Lvl1BunchGroupKey": "%s"%(bunchKey),
                        "ConfigureLvl1MenuFrom":"%s"%(L1MenuFrom)
                    }
                },
                "TriggerDBConnection": {
                    "TriggerDBConnection": {
                        "Type":"Coral",
                        "Server":"TRIGGERDB",
                        "SuperMasterKey":"%s"%SMK,
                        "User":"",
                        "Password":"",
                        "Name":"dummy",
                        "Alias":"TRIGGERDB"
                    }
                },
                "hlt": {
                    "HLTImplementationJobOptions": {
                        "UID": "HLTImplementationJobOptions-1",
                        "libraries": {
                            "library":["TrigServices", "TrigPSC"]
                        },
                        "jobOptionsPath":"%s"%(jopath),
                        "pythonSetupFile":"%s" % pythonSetupFile,
                        "logLevels":{
                            "logLevel":logLevels
                        },
                        "preCommands":{
                            "preCommand":preCmds
                        },
                        "postCommands":{
                            "postCommand":postCmds
                        },
                        "showInclude":"false",
                        "HLTCommonParameters": {
                            "HLTCommonParameters": {
                                "messageSvcType": msgType,
                                "jobOptionsSvcType" : joType,
                            }
                        }
                    }
                }
            }
        }

    def getTree(self):
        return recurseDict(self._defaultDict,"TriggerConfiguration")

class TriggerConfigDB(TriggerConfig):
    def __init__(self,SMK=0,L1PSK=0,L1BG=0,HPSK=0,Coral=False,DBAlias="TRIGGERDB",joType="TrigConf::HLTJobOptionsSvc",msgType="TrigMessageSvc"):
        TriggerConfig.__init__(self)
        self._SMK=SMK
        self._useCoral=Coral
        self._DBAlias=DBAlias
        self._defaultDict={
            "TriggerConfiguration": {
                "UID":"DBTriggerConfig-1",
                "L1TriggerConfiguration": {
                    "L1TriggerConfiguration": {
                        "UID":"L1TrigConf",
                        "Lvl1PrescaleKey":"%s" % L1PSK,
                        "Lvl1BunchGroupKey":"%s" % L1BG,
                        "ConfigureLvl1MenuFrom":"DB",
                    }
                },
                "hlt": {
                    "HLTImplementationDB": {
                        "UID":"HLTImplementationDB-1",
                        "libraries": {
                            "library": ["TrigServices", "TrigPSC", "TrigConfigSvc"],
                        },
                        "hltPrescaleKey":"%s"%(HPSK),
                        "HLTCommonParameters": {
                            "HLTCommonParameters": {
                                "messageSvcType":msgType,
                                "jobOptionsSvcType":joType,
                                "dllName":"",
                                "factoryName":""
                            }
                        }
                    }
                }
            }
        }

    def getTree(self):
        TC=recurseDict(self._defaultDict,"TriggerConfiguration")
        dbc=TriggerConfig.getDBConfig(self,self._SMK,coral=self._useCoral,alias=self._DBAlias)
        if dbc[0] is not None:
            TC.find("TriggerConfiguration").append(dbc[0])
        if dbc[1] is not None:
            TC.find("TriggerConfiguration").append(dbc[1])
        return TC

class TriggerConfigDBPython(TriggerConfigDB):
    def __init__(self,preCmds=[],postCmds=[],pythonSetupFile="TrigPSC/TrigPSCPythonDbSetup.py",logLevels=["INFO","ERROR"],**kwargs):
        TriggerConfigDB.__init__(self, **kwargs)

        # Some additional changes compared to our parent:
        tc = self._defaultDict["TriggerConfiguration"]
        # rename key
        tc["hlt"]["HLTImplementationDBPython"] = tc["hlt"].pop("HLTImplementationDB")
        # change UID
        tc["hlt"]["HLTImplementationDBPython"]["UID"] = "HLTImplementationDBPython-1"

        # add pre/postCommands
        tc["hlt"]["HLTImplementationDBPython"].update( {
            "preCommands":{
                "preCommand":preCmds
            },
            "postCommands":{
                "postCommand":postCmds
            }
        })

        # add athenaHLT configuration
        tc["athenaHLTSpecificConfiguration"] = {
            "logLevels": {
                "logLevel":logLevels
            },
            "pythonSetupFile":"%s" % pythonSetupFile
        }

    def getTree(self):
        return TriggerConfigDB.getTree(self)

# PuDummy configuration is fixed for now, ideally command line arguments should define some of this configuration
class TriggerConfigPuDummy(object):
    def __init__(self, probability=0.5):
        self._defaultDict={
            "TriggerConfiguration":
                {
                "UID": "PuDummyTriggerConfig-1",
                "hlt":
                    {
                    "PuDummySteering":
                        {
                        "UID": "HLT-Dummy-Steering",
                        "libraries":{
                            "library":["pudummy"]
                        },
                        "resultSizeDistribution": "C|1000",
                        "nbrHltTriggerInfo": "0",
                        "nbrPscErrors": "0",
                        "seed": "0",
                        "steps":
                            {
                            "DummyROBStep":
                                {
                                "UID": "calo_algorithm-1",
                                "burnTimeDistribution": "F|TMath::Gaus(x,20000,20)|5000|500000",
                                "debugTag":
                                    {
                                    "DummyStreamTag":
                                        {
                                        "UID": "L2-Debug-Tag-0",
                                        "name": "",
                                        "probability": str(probability)
                                        }
                                    },
                                "physicsTag":
                                    {
                                    "DummyStreamTag":
                                        {
                                        "UID": "L2-CaloPhysics-Tag-0",
                                        "name": "",
                                        "probability": str(probability)
                                        }
                                    },
                                "roi":
                                    {
                                    "DataAccess":
                                        {
                                        "UID": "accessAnySubdetector-1",
                                        "subDetector": "0",
                                        "nROBDistribution": "C|1",
                                        "comment": "Access 1 ROB in any sub detector"
                                        }
                                    }
                                },
                            "DummyStep":
                                {
                                "UID": "efStep",
                                "burnTimeDistribution": "C|500000",
                                "debugTag":
                                    {
                                    "DummyStreamTag":
                                        {
                                        "UID": "EF-Debug-Tag-0",
                                        "name": "",
                                        "probability": str(probability)
                                        }
                                    },
                                "physicsTag":
                                    {
                                    "DummyStreamTag":
                                        {
                                        "UID": "EF-Physics-Tag-0",
                                        "name": "",
                                        "probability": str(probability)
                                        }
                                    },
                                "calibrationTag":
                                    {
                                    "DummyCalStreamTag":
                                        {
                                        "UID": "EF-Calibration-Tag-0",
                                        "name": "EF-Calibration-Tag-0",
                                        "probability": str(probability),
                                        "calibData":
                                            {
                                            "DataAccess":
                                                {
                                                "UID": "accessAnySubdetector-1",
                                                "subDetector": "0",
                                                "nROBDistribution": "F|TMath::Gaus(x,2,2)|1|5",
                                                "comment": "1-5 ROBS from any sub detector"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

    def getTree(self):
        return  recurseDict(self._defaultDict,"TriggerConfiguration")

class HLTMPPUConfig(object):
    def __init__(self,numForks=2,numSlots=2,numThreads=2,finalizeTimeout=120,
                 hltresultSizeMb=10,
                 getNextTimeout=1000,
                 HardTimeout=60000,
                 softTimeoutFraction=0.8,
                 extraParams=[],
                 childLogRoot="/tmp/",
                 childLogName="",
                 DataSrc=None,
                 InfoSvc=None,
                 partitionName="test",
                 dontPublishIS=0,
                 HLTLibs=["TrigServices","TrigPSC","TrigConfigSvc"]):
        self._DataSource=DataSrc
        self._InfoService=InfoSvc
        self._childLogName=childLogName
        if len(childLogRoot)==0:
            self._childLogRoot="/log/%s/%s"%(os.environ["USER"],partitionName)
        if not os.path.exists(childLogRoot):
            try:
                os.mkdir(childLogRoot)
            except Exception:
                e = sys.exc_info()[0]
                log.warning("Log directory creation failed! %s", e)
        self._defaultDict={
            "UID":"HLTMPPy",
            "childLogName":self._childLogName,
            "numForks":numForks,
            "numberOfEventSlots":numSlots,
            "numberOfAthenaMTThreads":numThreads,
            "maximumHltResultMb":hltresultSizeMb,
            "getNextTimeout":getNextTimeout,
            "finalizeTimeout":finalizeTimeout,
            "HardTimeout":HardTimeout,
            "softTimeoutFraction":softTimeoutFraction,
            "extraParams":extraParams,
            "HLTImplementationLibraries":HLTLibs,
            "DataSource":self._DataSource,
            "InfoService":self._InfoService,
            "dontPublishIS":dontPublishIS
            }

    def getTree(self):
        root=et.Element("HLTMPPUApplication")
        specials=["extraParams","HLTImplementationLibraries","DataSource","InfoService"]
        dd=self._defaultDict
        for k in list(dd.keys()):
            if k not in specials:
                et.SubElement(root,str(k)).text=str(dd[k])
            else:
                if k =="extraParams":
                    if dd[k] is None or len(dd[k])==0:
                        continue
                    ep=et.SubElement(root,"extraParams")
                    for e in dd[k]:
                        et.SubElement(ep,"parameter").text=str(e)
                elif k=="HLTImplementationLibraries":
                    hl=et.SubElement(root,"HLTImplementationLibraries")
                    for l in dd[k]:
                        et.SubElement(hl,"library").text=str(l)
                elif k=="DataSource":
                    # dt=dd[k].getTree()
                    # root.append(dt)
                    et.SubElement(root,"DataSourceLibrary").text=str(dd[k].getLibrary())
                elif k=="InfoService":
                    # it=dd[k].getTree()
                    # root.append(dt)
                    et.SubElement(root,"InfoServiceLibrary").text=str(dd[k].getLibrary())
        root.append(self._DataSource.getTree())
        Inf=et.SubElement(root,"InfoService")
        Inf.append(self._InfoService.getTree())
        return root

class DummyRC(object):
    """ Dummy Run Control application, just implementing state transitions"""
    def __init__(self, ptree=""):
        log.info("Starting DummyRC")
        self.state="LOADED" # State may be used in interactive mode

    def Configure(self, ptree=""):
        self.state="CONFIGURED"

    def Unconfigure(self, ptree=""):
        self.state="LOADED"

    def Connect(self, ptree=""):
        self.state="CONNECTED"

    def Disconnect(self, ptree=""):
        self.state="CONFIGURED"

    def PrepareForRun(self, ptree=""):
        self.state="PREPARED"

    def StopRun(self, ptree=""):
        self.state="CONNECTED"
