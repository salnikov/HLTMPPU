#!/usr/bin/env tdaq_python
from __future__ import print_function
from __future__ import absolute_import
from builtins import str
from builtins import input
from builtins import object
import os
import sys
import time
import subprocess
import pprint
import signal
from . import HLTMPPy

import logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger('HLTMPPy')

class Infrastructure(object):
    """Class to manage online infrastructure"""

    sigs = [signal.SIGFPE, signal.SIGHUP, signal.SIGQUIT, signal.SIGSEGV, signal.SIGTERM, signal.SIGINT]

    def __init__(self, config_dictionary):
        super(Infrastructure, self).__init__()
        self.cdict = config_dictionary  # Dictionary that is returned from createFinalDict
        self.processes = []  # List of subprocess.Popen instances
        self.pid = os.getpid()  # Used to distinguish mother from children after forking
        self.register_handlers()

    def __del__(self):
        """ Stop infrastructure in the mother process, in case program exits before stop() is called """
        log.debug("Destructor of Infrastructure called")
        if os.getpid() == self.pid:   # True if we are in the mother process
            log.info("Destroying Infrastructure")
            self.stop()

    def register_handlers(self):
        self.prehandlers = {}
        for s in self.sigs:
            self.prehandlers[s] = signal.getsignal(s)  # Save previous handler for the same signal
            log.debug("Signal {}, Previous handler: {}".format(s, self.prehandlers[s]))
            signal.signal(s, self._handle_quit)

    def _handle_quit(self, signum, frame):
        log.error("Caught signal %d. Trying to clean the infrastructure and exit cleanly" % signum)
        self.__del__() # this doesn't delete the object, only executes __del__
        # execute the previous handler as well:
        prehandler = signal.SIG_DFL
        if signum in self.prehandlers:
            prehandler = self.prehandlers[signum]
            del self.prehandlers[signum]
        signal.signal(signum, prehandler)  # Register the previous handler, or SIG_DFL if not existant
        sys.exit(0)
        # os.kill(os.getpid(), signum)
        # This mechanism isn't working as expected. getsignal() only returns previous python signal
        # handlers, and not the ones defined in C++ libraries.

    def start(self):
        # This is a hack to ensure infrastructure processes will exit when parent dies
        from ctypes import cdll
        PR_SET_PDEATHSIG = 1
        try:
            implant_bomb = lambda: cdll['libc.so.6'].prctl(PR_SET_PDEATHSIG, signal.SIGKILL)
        except:  # Replace it with a dummy function
            log.error('Error setting PR_SET_PDEATHSIG for infrastructure processes. Using a dummy function instead')
            implant_bomb = lambda: 1

        from ispy import IPCPartition

        cdict = self.cdict

        log.info("Initializing OH Monitoring Infrastructure")

        # Start infrastructure, ipc and is servers. See stdout/stderr attributes for log files
        log.info("Initialising partition infrastructure")
        ipc_initial = subprocess.Popen(["ipc_server"], preexec_fn=implant_bomb,
                                       stdout=open("/tmp/initial_out_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                                       stderr=open("/tmp/initial_err_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID ipc_initial: {}".format(ipc_initial.pid))
        self.processes.append(ipc_initial)

        while not IPCPartition("initial").isValid():
            log.info("Wait until initial partition is available...")
            time.sleep(1)

        # Start partition
        log.info("Starting partition: {}".format(cdict['global']['partition_name']))
        ipc_partition = subprocess.Popen(["ipc_server","-p", cdict['global']['partition_name']], preexec_fn=implant_bomb,
                                         stdout=open("/tmp/part_{}_out_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                                         stderr=open("/tmp/part_{}_err_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID ipc_partition: {}".format(ipc_partition.pid))
        self.processes.insert(0,ipc_partition)

        while not IPCPartition(cdict['global']['partition_name']).isValid():
            log.info("Wait until partition {} is available...".format(cdict['global']['partition_name']))
            time.sleep(1)

        # Start relevant IS servers (DF, RunParams and Histogramming)
        log.info("Starting IS Repository: {}".format("DF"))
        is_df = subprocess.Popen(["is_server","-p", cdict['global']['partition_name'], "-n", "DF"], preexec_fn=implant_bomb,
                                 stdout=open("/tmp/isdf_out_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                                 stderr=open("/tmp/isdf_err_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID is_df: {}".format(is_df.pid))
        self.processes.append(is_df)

        log.info("Starting IS Repository: {}".format("RunParams"))
        is_runparams = subprocess.Popen(["is_server","-p", cdict['global']['partition_name'], "-n", "RunParams"], preexec_fn=implant_bomb,
                                        stdout=open("/tmp/isrunparams_out_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                                        stderr=open("/tmp/isrunparams_err_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID is_runparams: {}".format(is_runparams.pid))
        self.processes.append(is_runparams)

        log.info("Starting IS Repository: {}".format(cdict['monitoring']['OHServerName']))
        is_histo = subprocess.Popen(["is_server","-p", cdict['global']['partition_name'], "-n", cdict['monitoring']['OHServerName']], preexec_fn=implant_bomb,
                                    stdout=open("/tmp/ishisto_out_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                                    stderr=open("/tmp/ishisto_err_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID is_histo: {}".format(is_histo.pid))
        self.processes.append(is_histo)

        log.info("Starting RDB Server for IS Meta Data")
        rdb = subprocess.Popen(["rdb_server","-p",cdict['global']['partition_name'],"-d","ISRepository","-s","-D",getISInfoFile()], preexec_fn=implant_bomb,
                               stdout=open("/tmp/rdb_out_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),
                               stderr=open("/tmp/rdb_err_{}_{}".format(cdict['global']['partition_name'],int(time.time())),"w"),close_fds=True)
        log.debug("PID rdb: {}".format(rdb.pid))
        self.processes.append(rdb)

    def stop(self):
        if not self.processes:
            log.debug("There are no infrastructure processes to stop")
            return 0

        cdict = self.cdict
        log.info("Finalizing OH Monitoring Infrastructure")

        log.debug("Destroying partition: {}".format(cdict['global']['partition_name']))
        subprocess.call(['ipc_rm','-f','-p',cdict['global']['partition_name'],'-i','".*"','-n','".*"'],
                        stdout=subprocess.PIPE,stderr=subprocess.PIPE)

        log.debug("Destroying partition: {}".format("initial"))
        subprocess.call(['ipc_rm','-f','-p',"initial",'-i','".*"','-n','".*"'],
                        stdout=subprocess.PIPE,stderr=subprocess.PIPE)

        log.debug("Destroying IS servers: {}, {}, {}".format("DF","RunParams", cdict['monitoring']['OHServerName']))

        # In case the ipc_server and is_servers are still running, kill them
        for process in self.processes:
            pid = process.pid
            poll = process.poll()
            log.debug("PID: {}, alive: {}".format(pid, (poll==None)))
            while process.poll() is None:  # None means process is still alive
                process.kill()
                time.sleep(0.1)
        self.processes = []
        log.debug("Terminated all infrastructure processes")

    def copy_histograms(self):
        cdict = self.cdict
        log.info("Copying histograms into file")
        os.system('oh_cp -p%s -s%s -H -1 -O -r %d' % (cdict['global']['partition_name'],
                                                      cdict['monitoring']['OHServerName'],
                                                      cdict['global']['run_number']))

class Interact(object):
    """ Handles interactive mode"""
    _order = ['configure', 'connect', 'prepareForRun', 'start', 'stopRun', 'disconnect', 'unconfigure']

    _motions = {}
    _motions['LOADED']     = {'f': "configure",
                              'b': None}
    _motions['CONFIGURED'] = {'f': "connect",
                              'b': "unconfigure"}
    _motions['CONNECTED']  = {'f': "prepareForRun",
                              'b': "disconnect"}
    _motions['PREPARED']   = {'f': None,
                              'b': "stopRun"}

    def __init__(self, mppy, dcm, configure_dict, infr_handle):
        """
           mppy: HLTMPPy instance
           dcm : DCMPy instance
           configure_dict: Configuration dictionary
           infr_handle: Handle to Infrastructure class, required to kill
               infrastructure at exit.
        """
        super(Interact, self).__init__()
        self._mppy = mppy
        self._dcm = dcm
        self._cdict = configure_dict
        self._ctree = getConfigurationTree(configure_dict)
        self._prtree = getPrepareForRunTree(configure_dict)
        self._infr_handle = infr_handle
        self.start()

    def print_help(self):
        help_text = 'Valid commands are:'
        help_text +='\n\t<f>: move forward to the next state in chain'
        help_text +='\n\t<b>: move backward to the previous state in chain'
        help_text +='\n\t<e>: exit the interactive mode'
        help_text +='\n\t<x>: execute python commands'
        help_text +='\n\t<p>: start python prompt'
        help_text +='\n\t<#>: ignore this line'
        help_text +='\n\t<h>: print this help message'
        help_text +='\bAssumed State Transition chain order is: %s' % \
            ', '.join(self._order)
        print(help_text)

    def start(self):
        """ Start the interactive prompt
            At <e>xit command, infrastructure(if existent) is stopped
            and sys.exit() is called """
        log.info("Entering interactive mode")
        self.print_help()
        mppy = self._mppy
        dcm = self._dcm
        while True:
            prompt = ("State is '%s' (<f>orward, <b>ackward, "
                      "<p>rompt, e<x>ec, <h>elp, <#>comment, <e>xit)?\n")
            action = input(prompt % mppy.state).strip()
            action = action.split(' ', 1)

            if not action[0]: continue
            if action[0][0] not in ('f', 'b', 'e', 'x', 'p', '#', 'h'):
                log.info('Invalid command => `%s\'' % action[0][0])

            elif action[0] == 'h':
                self.print_help()

            elif action[0] == 'e':
                sys.exit(0)
            elif action[0] == 'p':

                log.info('Interactive python prompt: Use Ctrl-D to resume')

                import code
                code.interact(local=locals())

            elif action[0] == 'x':
                try:
                    exec(action[1])
                except Exception as e:
                    log.error('Error executing command: \'%s\'' % ' '.join(action[1:]))

            elif action[0][0] == '#':
                continue

            else:  # f or b options
                transition = self._motions[mppy.state][action[0][0]]  # transition to execute
                if not transition:
                    act = 'backward' if action[0][0] == 'b' else 'forward'
                    log.error('Moving %s is not allowed from state %s.' % (act, mppy.state))
                    continue
                if transition == "configure":
                    mppy.Configure(self._ctree)
                    dcm.Configure()
                elif transition == "connect":
                    mppy.Connect()
                    dcm.Connect()
                elif transition == "prepareForRun":
                    mppy.PrepareForRun(self._prtree)
                    dcm.PrepareForRun(self._prtree)
                elif transition == "stopRun":
                    waitUntilAllChildExited(mppy, self._cdict)
                    mppy.StopRun()
                    dcm.StopRun()
                elif transition == "disconnect":
                    mppy.Disconnect()
                    dcm.Disconnect()
                elif transition == "unconfigure":
                    mppy.Unconfigure()
                    dcm.Unconfigure()

def getDataSource(cdict):
    NS=cdict["datasource"]
    if NS is None or NS['module']=='dcmds':
        from .HLTMPPy import DCMDataSource as ds
        return ds()
    if NS['module']=='dffileds':
        from .HLTMPPy import DFFileDataSource as FD
        fds=FD(fileList=NS['file'],
               outFile=NS["outFile"],
               rosHitStatFile=NS["rosHitStatFile"],
               compressionLevel=NS["compressionLevel"],
               compressionFormat=NS["compressionFormat"],
               preload=NS["preload"],
               numEvents=NS["numEvents"],
               library=NS["dslibrary"],
               loopFiles=NS["loopFiles"],
               extraL1Robs=NS["extraL1Robs"],
               skipEvents=NS["skipEvents"]
               )
        return fds
    return None

def getInfoSvc(cdict):
    NS=cdict['monitoring']
    if NS is None:
        from .HLTMPPy import MonSvcInfoService as MSI
        mon=MSI()
        return mon
    if NS['module']=="monsvcis":
        from .HLTMPPy import MonSvcInfoService as MSI
        mon=MSI(OHServer=NS['OHServer'],
                OHSlots=NS['OHSlots'],
                OHInterval=NS['OHInterval'],
                OHInclude=NS['OHInclude'],
                OHExclude=NS['OHExclude'],
                ISServer=NS['ISServer'],
                ISSlots=NS['ISSlots'],
                ISInterval=NS['ISInterval'],
                ISRegex=NS['ISRegex'])
        return mon
    return None

def getTriggerConfig(cdict):
    td=cdict['trigger']
    if td['module']=='joboptions':
        from .HLTMPPy import TriggerConfigJO as TC
        tc=TC(jopath=td['joFile'],
              joType=td['joType'],
              msgType=td['msgType'],
              logLevels=td['logLevels'],
              pythonSetupFile=td['pythonSetupFile'],
              preCmds=td['precommand'],
              postCmds=td['postcommand'])
        return tc
    elif td['module']=='DB':
        from .HLTMPPy import TriggerConfigDB as TC
        tc=TC(SMK=td['SMK'],
              L1PSK=td['l1PSK'],
              L1BG=td['l1BG'],
              HPSK=td['HLTPSK'],
              Coral=td['use_coral'],
              joType=td['joType'],
              msgType=td['msgType'],
              DBAlias=td['db_alias'])
        return tc
    elif td['module']=='DBPython':
        from .HLTMPPy import TriggerConfigDBPython as TC
        tc=TC(SMK=td['SMK'],
              L1PSK=td['l1PSK'],
              L1BG=td['l1BG'],
              HPSK=td['HLTPSK'],
              Coral=td['use_coral'],
              preCmds=td['precommand'],
              postCmds=td['postcommand'],
              joType=td['joType'],
              msgType=td['msgType'],
              logLevels=td['logLevels'],
              DBAlias=td['db_alias'],
              pythonSetupFile=td['pythonSetupFile']
        )
        return tc
    elif td['module']=='pudummy':
        from .HLTMPPy import TriggerConfigPuDummy as TC
        tc=TC(probability = td["probability"])
        return tc
    return None

def getHLTMPPUConfig(cdict,DS=None,IS=None):
    hd=cdict['HLTMPPU']
    DSrc=DS
    if DSrc is None : DSrc=getDataSource(cdict)
    ISvc=IS
    if ISvc is None : ISvc=getInfoSvc(cdict)

    from .HLTMPPy import HLTMPPUConfig as HC
    hc=HC(numForks=hd['num_forks'],
          numSlots=hd['num_slots'],
          numThreads=hd['num_threads'],
          hltresultSizeMb=hd['hltresultSizeMb'],
          HardTimeout=hd['hard_timeout'],
          softTimeoutFraction=hd['soft_timeout_fraction'],
          extraParams=hd['extra_params'],
          childLogRoot=hd['log_root'],
          childLogName=hd['log_name'],
          partitionName=hd['partition_name'],
          HLTLibs=cdict['trigger']['library'],
          dontPublishIS=hd['dontPublishIS'],
          DataSrc=DSrc,
          InfoSvc=ISvc
      )
    return hc

def getISInfoFile():
    """ Return full path of the HLTMPPU IS Info file in TDAQ_DB_PATH """
    paths = os.environ["TDAQ_DB_PATH"].split(":")
    info_file_path = "/HLTMPPU/schema/HLTMPPU_ISInfo.schema.xml"
    for path in paths:
        full_path = path + info_file_path
        if os.path.exists(full_path):
            log.info("Found IS Info file at: " + full_path)
            return full_path

    log.error("Can not find {} in TDAQ_DB_PATH: {}, exiting".format(info_file_path, paths))
    sys.exit(0)

def getPartitionTree(cdict):
    import xml.etree.ElementTree as et
    root=et.Element("Partition")
    et.SubElement(root,"UID").text=str(cdict['global']['partition_name'])
    et.SubElement(root,"LogRoot").text=str(cdict['global']['log_root'])
    return root

def getConfigurationTree(cdict):
    import xml.etree.ElementTree as et
    root=et.Element("Configuration")
    ds=getDataSource(cdict)
    inf=getInfoSvc(cdict)
    hlt=getHLTMPPUConfig(cdict,ds,inf)
    trig=getTriggerConfig(cdict)
    part=getPartitionTree(cdict)
    part.append(trig.getTree())
    root.append(hlt.getTree())
    root.append(part)

    from .HLTMPPy import genRosMapping
    try:
        ros2robs = genRosMapping(cdict["global"]["ros2robs"])
    except Exception as ex:
        log.error("Problem reading ros2robs due to : {}".format(ex))
        log.error("ros2robs : {}".format(cdict["global"]["ros2robs"]))
        raise
    root.append(ros2robs)
    return HLTMPPy.prettyxml(root)

def getPrepareForRunTree(cdict):
    RunParams={
      "run_number":cdict['global']['run_number'],
      'max_events':'0',
      'recording_enabled':'0',
      'trigger_type':cdict['global']['trigger_type'],
      'run_type':"Physics",
      'det_mask':'0'*(32-len(cdict['global']['detector_mask']))+cdict['global']['detector_mask'],
      'det_mask':cdict['global']['detector_mask'].rjust(32,"0"),
      'beam_type':cdict['global']['beam_type'],
      'beam_energy':cdict['global']['beam_energy'],
      'stream':cdict['global']['stream'],
      'lumiblock':cdict['global']['lumiblock'],
      'filename_tag':"",
      'T0_project_tag':cdict['global']['T0_project_tag'],
      'timeSOR':cdict['global']['date'],
      'timeEOR':'1/1/70 01:00:00',
      'totalTime':'0'
    }
    Magnets={'ToroidsCurrent':
          {
           'value':cdict['global']['toroid_current'],
           'ts':cdict['global']['date']
          },
          'SolenoidCurrent':
          {
           'value':cdict['global']['solenoid_current'],
           'ts':cdict['global']['date']
          }
    }
    import xml.etree.ElementTree as et
    RT=et.Element("RunParams")
    for k in iter(RunParams):
        et.SubElement(RT,k).text=str(RunParams[k])
    M=et.Element("Magnets")
    for m in ("ToroidsCurrent","SolenoidCurrent"):
        T=et.SubElement(M,m)
        for k in iter(Magnets[m]):
            et.SubElement(T,k).text=str(Magnets[m][k])
    return HLTMPPy.prettyxml(RT) + "\n" + HLTMPPy.prettyxml(M)

def waitUntilAllChildExited(mppy, config_dictionary):
    """
       Wait until the IS information NumActive is 0

       mppy: HLTMPPy instance
       config_dictionary: Dictionary that is returned from createFinalDict
    """

    log.info("Waiting until all children exited...")
    while mppy.NumberOfActiveChildren() != 0:
      time.sleep(1)
    log.info("All children exited...")

def createFinalDict(input_dict):
    """ Create final dictionary for configuration.
        This function evaluates the dictionary created by command line parser,
        and changes some elements. """

    cdict = input_dict

    cdict['monitoring']['OHServer'] = '${{TDAQ_OH_SERVER={}}}'.format(cdict['monitoring']['OHServerName'])

    cdict['global']['stream'] = ""
    cdict['global']['lumiblock'] = ""
    # If we are using File based Data Source, get all run properties
    if cdict['datasource']['module']=='dffileds':
        first_file = cdict['datasource']['file'][0]
        getRunParamsFromFile(cdict['global'], first_file)

    if cdict['global']['date'] is None:
        import datetime
        now=datetime.datetime.now()
        cdict['global']['date']="{:%d/%m/%y %H:%M:%S}".format(now)

    multi_event = input_dict['HLTMPPU']['num_forks']>1 or input_dict['HLTMPPU']['num_slots']>1
    if multi_event and input_dict['datasource']['dslibrary']!="DFDcmEmuBackend":
        log.error("Exiting... Running with multiple forks/slots require DFDcmEmuBackend. "
        "Actual backend: {}".format(input_dict['datasource']['dslibrary']))
        sys.exit(0)

    if "rosHitStatFile" not in input_dict['datasource']:
      cdict['datasource']['rosHitStatFile']="ros_hitstats"

    if 'hltresultSizeMb' not in input_dict['HLTMPPU']:
      cdict['HLTMPPU']['hltresultSizeMb'] = 10

    # Avoid IS publication warnings in case we run without infrastructure
    if cdict['global']['with_infrastructure']:
      cdict['HLTMPPU']['dontPublishIS'] = 0
    else:
      cdict['HLTMPPU']['dontPublishIS'] = 1

    return cdict

def getRunNumberFromFirstEvent(filename):
    """Get Run number from the first event in the file"""
    import eformat
    stream = eformat.istream(filename)
    return stream.__iter__().next().run_no()

def getRunParamsFromFile(global_dict, filename):
    """ Update global_dict with following run parameters from first input file: run number,
          detector mask, T0_project_tag, LB, trigger_type, beam_type, beam_energy, stream

      Arguments:
      global_dict -- global dictionary to be updated
      filename -- Name of the first input file
    """
    import libpyevent_storage as EventStorage
    reader = EventStorage.pickDataReader(filename)
    if global_dict['run_number'] == 0:
        global_dict['run_number'] = reader.runNumber()
    if global_dict['detector_mask'] == "":
        global_dict['detector_mask'] = "{:032x}".format(reader.detectorMask())

    global_dict['stream'] = reader.stream()
    global_dict['lumiblock'] = reader.lumiblockNumber()
    global_dict['T0_project_tag'] = reader.projectTag()
    global_dict['beam_energy'] = reader.beamEnergy()
    global_dict['beam_type'] = reader.beamType()
    global_dict['stream'] = reader.stream()
    global_dict['trigger_type'] = reader.triggerType()


def hookDebugger(follow='parent'):
    """Hook debugger to parent/child"""
    import os
    pid = os.spawnvp(os.P_NOWAIT, 'gdb', ['gdb', '-q', '-ex', 'set follow-fork-mode %s' % follow,
                                          'python', str(os.getpid())])

    # give debugger some time to attach to the python process
    import time
    time.sleep(1)

    # verify the process' existence (will raise OSError if failed)
    os.waitpid(pid, os.WNOHANG)
    os.kill(pid, 0)
    return


def runHLTMPPy(runHLTMPPy_dict):
    """ Main function to run the standalone HLTMPPU script

      config_tree is either created by command line parser of runHLTMPPy.py
      or athenaHLT.py """

    cdict = createFinalDict(runHLTMPPy_dict)
    pp=pprint.PrettyPrinter()
    log.info("Final runHLTMPPy dict:")
    pp.pprint(cdict)

    from os import environ as env
    if "TDAQ_APPLICATION_NAME" not in env:
        env["TDAQ_APPLICATION_NAME"]=cdict['HLTMPPU']['application_name']
    if "TDAQ_PARTITION" not in env:
        env["TDAQ_PARTITION"]=cdict['global']['partition_name']
    log.info("configure tree:")
    tree=getConfigurationTree(cdict)
    print(tree)
    sys.stdout.flush()
    log.info("prepareForRun tree:")
    prtree=getPrepareForRunTree(cdict)
    print(prtree)
    sys.stdout.flush()

    # Set local ipc_init.ref file
    ipc_ref = os.path.join(os.getcwd(), 'ipc_init.ref')
    log.info("Setting TDAQ_IPC_INIT_REF: {}".format(ipc_ref))
    os.environ['TDAQ_IPC_INIT_REF'] = 'file:' + ipc_ref

    infr = Infrastructure(cdict) # Initialize infrastructure class in case it's needed

    import libHLTMPPy_boost  # Library with HLTMPPU, HLTInterface and DcmEmulator Python Bindings

    # Initialize a dummy DCMPy instance if we run with another backend
    from .HLTMPPy import DummyRC
    dcm = DummyRC(tree)
    if cdict['datasource']['module']  == 'dffileds':
      if cdict['datasource']['dslibrary'] == "DFDcmEmuBackend":
        log.info("Starting DCM")
        dcm=libHLTMPPy_boost.DcmEmulatorPy(tree)

    if cdict['global']['with_infrastructure']:
        infr.start()


    mppy=libHLTMPPy_boost.HLTMPPUPy()

    from ispy import IPCPartition # Without this, one gets IPC Not Initialized error

    if cdict['HLTMPPU']['interactive']: # After interactive mode, program exits
        interact = Interact(mppy = mppy,
                            dcm = dcm,
                            configure_dict = cdict,
                            infr_handle = infr)

    if cdict['HLTMPPU'].get('debug')=='parent':
        hookDebugger('parent')

    mppy.Configure(tree)
    dcm.Configure()

    mppy.Connect()
    dcm.Connect()

    if cdict['HLTMPPU'].get('debug')=='child':
        hookDebugger('child')

    # We have to run HLTMPPU.PrepareForRun first, because it will fork children
    mppy.PrepareForRun(prtree)

    dcm.PrepareForRun(prtree)

    waitUntilAllChildExited(mppy, cdict)

    # At the end of run, copy histograms
    if cdict['global']['with_infrastructure']:
        infr.copy_histograms()

    # Now we know that all children finished processing , we can call safely call stopRun()
    mppy.StopRun()
    dcm.StopRun()
    mppy.Disconnect()
    dcm.Disconnect()
    mppy.Unconfigure()
    dcm.Unconfigure()

    infr.stop()
