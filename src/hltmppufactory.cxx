// Dear emacs, this is -*- c++ -*-
#include "HLTMPPU/HLTMPPU.h"

extern "C" hltinterface::HLTInterface* create_interface(){
  return new HLTMPPU();
}

extern "C" void destroy_interface(hltinterface::HLTInterface* i){
  auto h=dynamic_cast<HLTMPPU*>(i);
  delete h;
}
