#include "HLTMPPU/MonSvcInfoService.h"

extern "C" hltinterface::IInfoRegister* create_hltmp_infoservice(){
  return new HLTMP::MonSvcInfoService();
}

extern "C" void destroy_hltmp_infoservice(hltinterface::IInfoRegister* i){
  HLTMP::MonSvcInfoService* k=reinterpret_cast<HLTMP::MonSvcInfoService*>(i);
  delete k;
}
