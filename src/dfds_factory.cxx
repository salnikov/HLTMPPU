#include "HLTMPPU/DFDataSource.h"

extern "C" HLTMP::DataSource* create_hltmp_datasource(){
  return new HLTMP::DFDataSource();
}

extern "C" void destroy_hltmp_datasource(HLTMP::DataSource* i){
  HLTMP::DFDataSource* k=reinterpret_cast<HLTMP::DFDataSource*>(i);
  delete k;
}
