#include "HLTMPPU/DcmEmulator/DFDcmEmuEvent.h"
#include "HLTMPPU/DcmEmulator/DFDcmEmuSession.h"
#include "ers/ers.h"
#include "eformat/eformat.h"
#include "eformat/index.h"
#include "eformat/SourceIdentifier.h"
#include "eformat/FullEventFragmentNoTemplates.h"

uint32_t HLTMP::DFDcmEmuEvent::l1Id(){
  return m_currEvent->lvl1_id();
}

void HLTMP::DFDcmEmuEvent::getL1Result(std::unique_ptr<uint32_t[]>& l1Result){

  std::map<eformat::SubDetector, std::vector<const uint32_t*> > sd_toc;
  eformat::helper::build_toc(*m_currEvent, sd_toc);
  for (auto &[subdet, robs] : sd_toc) {
    switch (subdet) {
      case eformat::TDAQ_BEAM_CRATE:
      case eformat::TDAQ_SFI:
      case eformat::TDAQ_SFO:
      case eformat::TDAQ_EVENT_FILTER:
      case eformat::OTHER:
        //we ignore these
        break;
      case eformat::TDAQ_L2SV:
      case eformat::TDAQ_CALO_PREPROC:
      case eformat::TDAQ_CALO_CLUSTER_PROC_DAQ:
      case eformat::TDAQ_CALO_CLUSTER_PROC_ROI:
      case eformat::TDAQ_CALO_JET_PROC_DAQ:
      case eformat::TDAQ_CALO_JET_PROC_ROI:
      case eformat::TDAQ_MUON_CTP_INTERFACE:
      case eformat::TDAQ_CTP:
        //these, we treat as Level-1 result
	m_l1r.insert(m_l1r.end(), robs.begin(), robs.end());
        break;
      default:
        //these, we include in our internal data map for fast access
        for(auto & robptr : robs) {
          eformat::ROBFragment<const uint32_t*> rob(robptr);

          // Special case: ROB is one of extra L1 ROBs. Threat these as L1 result too
          if (std::find(m_extraL1Robs.begin(),m_extraL1Robs.end(), rob.source_id())!= m_extraL1Robs.end()){
            ERS_DEBUG(4, "Matching extra L1 Rob: " << rob.source_id());
            m_l1r.push_back(rob);
            continue;
          }
          //if I already have an entry there...
          if (m_IDmap.find(rob.source_id()) != m_IDmap.end()) {
            ERS_DEBUG(1,"Duplicate ROBS in file. Ignoring");
          }
          else m_IDmap[rob.source_id()] = robptr;
        }
        break;
    }
  }

  eformat::write::FullEventFragment l1Result_w;
  // Copy necessary header information 
  l1Result_w.bc_id(m_currEvent->bc_id());
  l1Result_w.global_id(m_currEvent->global_id());
  l1Result_w.lvl1_id(m_currEvent->lvl1_id());
  l1Result_w.bc_time_seconds(m_currEvent->bc_time_seconds());
  l1Result_w.bc_time_nanoseconds(m_currEvent->bc_time_nanoseconds());
  l1Result_w.run_no(m_currEvent->run_no());
  l1Result_w.lumi_block(m_currEvent->lumi_block());
  l1Result_w.lvl1_trigger_type(m_currEvent->lvl1_trigger_type());
  l1Result_w.lvl1_trigger_info(m_currEvent->nlvl1_trigger_info(), m_currEvent->lvl1_trigger_info());


  m_l1robs.reserve(m_l1r.size());  // This is necessary because otherwise there will be
                                   // extra move operations when vector resizes

  for(size_t t=0;t<m_l1r.size();t++){
    ERS_DEBUG(6, "Adding L1 fragment " << t << " of " << m_l1r.size() << ", Source ID : " << m_l1r.at(t).source_id());
    try {
      ERS_DEBUG(7, "Rob fragment [0] = " <<  m_l1r.at(t).start()[0]);
      m_l1robs.emplace_back(m_l1r.at(t).start()); // Create write::ROBFragment from the read::ROBFragment and put it in map
    } catch (std::exception & ex) {
      ERS_LOG("Exception: " << ex.what());
    } catch (...) {
      ERS_LOG("Exception: ?");
    }

    try {
      ERS_DEBUG(4, "Appending fragment at: " << &(m_l1robs.back()));
      l1Result_w.append(&(m_l1robs.back()));
    } catch (std::exception & ex) {
      ERS_LOG("Exception: " <<ex.what());
    } catch (...) {
      ERS_LOG("Exception: ?");
    }
  }

  const eformat::write::node_t* top = l1Result_w.bind();
  auto l1r_size = l1Result_w.size_word();
  uint32_t* l1r_read = new uint32_t[l1r_size];
  auto res = eformat::write::copy(*top, l1r_read, l1r_size);
  if (res != l1r_size) {
    std::string msg = "DFDataSource Caught exception: Event serialization failed!!";
    throw daq::dfinterface::CommunicationError(ERS_HERE, msg);
  }
  l1Result.reset(l1r_read);
}

void HLTMP::DFDcmEmuEvent::mayGetRobs(const std::vector<uint32_t>& robIds){

  ERS_DEBUG(1," mayGetRobs number of ROBs for prefetching = " << robIds.size());
  std::vector<std::pair<uint32_t, double>> robid_size;

  for (auto & robid : robIds) {
    if (m_IDmap.find(robid) == m_IDmap.end()) {
      char buff[400];
      snprintf(buff,400,"Event with LVL1 id=%u does NOT contain ROB 0x%08x",
	       m_currEvent->lvl1_id(),robid);
      ERS_DEBUG(1, buff);
      continue;
    }
    auto robptr = m_IDmap[robid];
    eformat::ROBFragment<const uint32_t*> rob(robptr);
    robid_size.push_back(std::make_pair(robid, rob.rod_fragment_size_word()*4.)); // Convert words to bytes
  }
  m_session.addPrefetchInfo(robid_size);
}

void HLTMP::DFDcmEmuEvent::getRobs(const std::vector<uint32_t>& robIds,
				 std::vector<hltinterface::DCM_ROBInfo>& robs){

  ERS_DEBUG(1," getRobs number of ROBs to get = " << robIds.size());  
  std::lock_guard<std::mutex> lck(m_getrob_mutex);
  std::vector<std::pair<uint32_t, double>> robid_size;

  for (auto & robid : robIds) {
    if (m_IDmap.find(robid) == m_IDmap.end()) {
      char buff[400];
      snprintf(buff,400,"Event with LVL1 id=%u does NOT contain ROB 0x%08x",
               m_currEvent->lvl1_id(), robid);
      ERS_DEBUG(1, buff);
      continue;
    }
    auto start = std::chrono::steady_clock::now();
    auto robptr = m_IDmap[robid];

    eformat::ROBFragment<const uint32_t*> rob(robptr);
    robid_size.push_back(std::make_pair(robid, rob.rod_fragment_size_word()*4.)); // Convert words to bytes
 
    if (m_collectedRobs.find(robid) != m_collectedRobs.end()) { // ROB was already collected, served from cache
      ERS_DEBUG(1," getRobs ROB id = 0x" << std::hex << robid << std::dec << " found in DCM cache ");
      // this will create a copy of the ROBFragment, but so what?
      robs.push_back(hltinterface::DCM_ROBInfo(robptr,
                                               true,
                                               start,
                                               std::chrono::steady_clock::now()));
    } else { // ROB was for the first time retrieved from the network
      ERS_DEBUG(1," getRobs ROB id = 0x" << std::hex << robid << std::dec << " retrieved from ROS ");
      m_collectedRobs.insert(robid);
      // mark also all ROBs which get prefetched with this ROB
      m_session.updateRobCacheInfo(robid, m_collectedRobs);
      // this will create a copy of the ROBFragment, but so what?
      robs.push_back(hltinterface::DCM_ROBInfo(robptr,
                                               false,
                                               start,
                                               std::chrono::steady_clock::now()));
    }
  }
  m_session.addRosHitInfo(robid_size, false);
}

void HLTMP::DFDcmEmuEvent::getAllRobs(std::vector<hltinterface::DCM_ROBInfo>& robs){

  ERS_DEBUG(1," getAllRobs ");  
  std::lock_guard<std::mutex> lck(m_getrob_mutex);
  std::vector<std::pair<uint32_t, double>> robid_size;
  for (auto & [robid,robptr] : m_IDmap) {
    if(m_collectedRobs.find(robid)==m_collectedRobs.end()){
      auto start = std::chrono::steady_clock::now();
      eformat::ROBFragment<const uint32_t*> rob(robptr);
      robid_size.push_back(std::make_pair(robid, rob.rod_fragment_size_word()*4.)); // Convert words to bytes
      m_collectedRobs.insert(robid);
      // missing ROB was retrieved from the network
      robs.push_back(hltinterface::DCM_ROBInfo(robptr,
    					       false,
    					       start,
    					       std::chrono::steady_clock::now()));
    }
  }
  m_session.addRosHitInfo(robid_size, true);
}

HLTMP::DFDcmEmuEvent::~DFDcmEmuEvent(){
}

HLTMP::DFDcmEmuEvent::DFDcmEmuEvent(std::unique_ptr<const uint32_t[]> b, DFDcmEmuSession & session, uint64_t gid):
  m_blob(std::move(b)), m_session(session), m_gid(gid){
  m_currEvent=std::make_shared<const eformat::read::FullEventFragment>(m_blob.get());
  ERS_DEBUG(3,"Address of read event blob: 0x" << std::hex << reinterpret_cast<std::uintptr_t>(m_blob.get()));
  m_lb = m_currEvent->lumi_block();
  m_gid = m_currEvent->global_id();
  m_collectedRobs.clear(); // clear the DCM event cache
}

uint64_t HLTMP::DFDcmEmuEvent::gid(){
  return m_gid;
}

uint16_t HLTMP::DFDcmEmuEvent::lumiBlock(){
  return m_lb;
}
